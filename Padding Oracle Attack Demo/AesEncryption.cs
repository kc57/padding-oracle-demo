﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Text;
using System.Security.Cryptography;
using System.IO;

namespace Padding_Oracle_Attack_Demo
{
    public class AesEncryption
    {
        public static byte[] generateKey(string password, string salt, int keySize)
        {
            byte[] saltBytes = Encoding.ASCII.GetBytes(salt);
            Rfc2898DeriveBytes key = new Rfc2898DeriveBytes(password, saltBytes);
            return key.GetBytes(keySize / 8);
        }

        public static string ByteArrayToString(byte[] ba)
        {
            string hex = BitConverter.ToString(ba);
            return hex.Replace("-", "");
        }

        public static byte[] StringToByteArray(string hex)
        {
            int NumberChars = hex.Length;
            byte[] bytes = new byte[NumberChars / 2];
            for (int i = 0; i < NumberChars; i += 2)
                bytes[i / 2] = Convert.ToByte(hex.Substring(i, 2), 16);
            return bytes;
        }

        public static string Encrypt(string plainText, string password)
        {
            string salt = "s@1tValue";
            string initVector = "1234567890123456"; // must be 16 bytes
            int keySize = 128;

            // Get into byte arrays
            byte[] plainTextBytes = Encoding.UTF8.GetBytes(plainText);
            byte[] initVectorBytes = Encoding.ASCII.GetBytes(initVector);

            // Generate the key from the password
            byte[] keyBytes = AesEncryption.generateKey(password, salt, keySize);
            
            byte[] cipherBytes = AesEncryption.Encrypt(plainTextBytes, keyBytes, initVectorBytes);

            //Append the hex bytes of the IV and cipher text
            string cipherText = ByteArrayToString(initVectorBytes) + ByteArrayToString(cipherBytes);

            return cipherText;
        }


        public static byte[] Encrypt(byte[] plainTextBytes, byte[] keyBytes, byte[] initVectorBytes)
        {
            AesCryptoServiceProvider aesProvider = new AesCryptoServiceProvider();
            aesProvider.Mode = CipherMode.CBC;
            ICryptoTransform encryptor = aesProvider.CreateEncryptor(keyBytes, initVectorBytes);

            byte[] cipherTextBytes;

            using (MemoryStream memoryStream = new MemoryStream())
            using(CryptoStream cryptoStream = new CryptoStream(memoryStream, encryptor, CryptoStreamMode.Write))
            {
                cryptoStream.Write(plainTextBytes, 0, plainTextBytes.Length);
                cryptoStream.FlushFinalBlock();
                cipherTextBytes = memoryStream.ToArray();

                // Close both streams.
                memoryStream.Close();
                cryptoStream.Close();
            }

            // Return encrypted byte array
            return cipherTextBytes;
        }

        public static string Decrypt(string cipherText, string password)
        {
            string salt = "s@1tValue";
            //string initVector = "@1B2c3D4e5F6g7H8"; // must be 16 bytes
            int keySize = 128;

            int len = (keySize / 8) * 2;

            string initVector = cipherText.Substring(0, len);
            cipherText = cipherText.Substring(len);

            // Get into byte arrays
            byte[] cipherTextBytes = StringToByteArray(cipherText);
            byte[] initVectorBytes = StringToByteArray(initVector);

            // Generate the key from the password
            byte[] keyBytes = AesEncryption.generateKey(password, salt, keySize);

            byte[] plainTextBytes = AesEncryption.Decrypt(cipherTextBytes, keyBytes, initVectorBytes);

            string plainText = Encoding.UTF8.GetString(plainTextBytes);

            return plainText;
        }

        public static byte[] Decrypt(byte[] cipherTextBytes, byte[] keyBytes, byte[] initVectorBytes)
        {
            AesCryptoServiceProvider aesProvider = new AesCryptoServiceProvider();
            aesProvider.Mode = CipherMode.CBC;
            ICryptoTransform decryptor = aesProvider.CreateDecryptor(keyBytes, initVectorBytes);

            byte[] plainTextBytes = new byte[cipherTextBytes.Length];

            using (MemoryStream memoryStream = new MemoryStream(cipherTextBytes))
            using(CryptoStream cryptoStream = new CryptoStream(memoryStream, decryptor, CryptoStreamMode.Read))
            {
                int decryptedByteCount = cryptoStream.Read(plainTextBytes, 0, plainTextBytes.Length);

                // Close both streams
                memoryStream.Close();
                cryptoStream.Close();
            }
        
            // Return decrypted byte array.   
            return plainTextBytes;
        }

    }
}