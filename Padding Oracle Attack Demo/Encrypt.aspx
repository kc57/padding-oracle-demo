﻿<%@ Page Title="Encrypt" Language="C#" MasterPageFile="~/Site.Master" AutoEventWireup="true" CodeBehind="Encrypt.aspx.cs" Inherits="Padding_Oracle_Attack_Demo.WebForm2" %>
<asp:Content ID="Content1" ContentPlaceHolderID="HeadContent" runat="server">
</asp:Content>
<asp:Content ID="Content2" ContentPlaceHolderID="MainContent" runat="server">
    <h1>Encrypt Data For Demo</h1>
<p>
    <asp:Label ID="Label1" runat="server" Text="Password:"></asp:Label>
    <asp:TextBox ID="txtPassword" runat="server" Width="378px"></asp:TextBox>
</p>
<p>
    <asp:Label ID="Label2" runat="server" Text="Plain Text:"></asp:Label>
    <asp:TextBox ID="txtPlainText" runat="server" Width="378px"></asp:TextBox>
</p>
<p>
    <asp:Label ID="Label3" runat="server" Text="Cipher Text:"></asp:Label>
    <asp:TextBox ID="txtCipherText" runat="server" Width="372px"></asp:TextBox>
</p>
<p>
    <asp:Button ID="cmdEncrypt" runat="server" onclick="cmdEncrypt_Click" 
        Text="Encrypt" />
</p>

</asp:Content>
