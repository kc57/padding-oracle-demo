﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.UI;
using System.Web.UI.WebControls;
using System.Text;

namespace Padding_Oracle_Attack_Demo
{
    public partial class WebForm2 : System.Web.UI.Page
    {
        protected void Page_Load(object sender, EventArgs e)
        {

        }

        protected void cmdEncrypt_Click(object sender, EventArgs e)
        {
            string plainText = txtPlainText.Text;
            string password = txtPassword.Text;
            
            txtCipherText.Text = AesEncryption.Encrypt(plainText, password);
        }
    }
}