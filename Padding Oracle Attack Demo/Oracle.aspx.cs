﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.UI;
using System.Web.UI.WebControls;
using System.Text;

namespace Padding_Oracle_Attack_Demo
{
    public partial class WebForm1 : System.Web.UI.Page
    {
        protected void Page_Load(object sender, EventArgs e)
        {
            string cipherText = Request.QueryString["cipher"];
            if (cipherText != null)
            {
                string password = "password";
                string plainText = AesEncryption.Decrypt(cipherText, password);
            }
        }
    }
}